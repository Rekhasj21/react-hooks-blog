# React

### 1. Can you explain the advantages of using JSX?

- React JS introduced a new HTML like syntax named JSX to create elements.
- JSX helps us in keeping our code simpler when writing large piece of code.
- JSX allows us to write HTML elements in Javascript and place them in DOM without any createElement() and appendChlid() methods
- JSX also allows React to show more useful error and warnings messages.
- If one is familiar with HTML, it is quite easy to use JSX when building React application.

### 2. Can you explain what is React Element?

- React Element are the basic buildings blocks of React applications.
- It is used to describe the what you want to see on the screen.
- The React.createElement() method is used to create an element using React JS. It is similar to the document.createElement() method in regular JavaScript.

  **Syntax:**

  `React.createElement(type, props);`

  type - Tag names like div, h1 and p, etc.
  props - Properties like className, onClick and id, etc. and its optional

- React JS introduced a new HTML like syntax named JSX to create elements.

**Syntax**

`const element = <h1 className="greeting">Hello World</h1>;`

- React elements are light weight and immutable.
- Once you create a element you cannot change its properties.
- It can be nested inside each other to create the UI of your application.

### 3. Can you list all the situation in which React re-renders the component?

- Props Change
- State Change
- Context Change
- Parent Component re-renders

React re-renders component only when necessary that is if any one of the above condition is statisfied a component is not re-rendered if React decides its not neccessary.

### 4. Can you explain when we need a functional and when we need a class component?

**Class Components**

- Use Class components when you need to manage state or use life cycle methods such as **componentDidMount** or **shouldComponentUpdate**.
- Use when need to implement more complex logic and functionality

**Functional Components**

- Use when you only need to render UI based on props or state
- Use when you don't use lifecycle methods or manage state.
- Use when to keep your code concise and easy read.

With the introduction of React Hooks, functional components can now also have state and lifecycle methods, which makes them even more versatile.

### 5. Can you explain the difference between a functional and class component?

- Functional components are same as JavaScript functions, which take in props as input and return React element as output
- They are simpler to write, easier to read, and offer better performance than class components.
- Renders the UI based on props

**Syntax**

```
const Welcome = () => <h1>Hello, User</h1>;

export default Welcome;
```

- Class components are defined using ES6 class
- To define a React Class Component,
  - Create an ES6 class that extends React.Component.
  - Add a single empty method to it called render().
- **extends** keyword is used to inherit methods and properties from the React.Component.
- **render()** method is the only required method in a class component. It returns the JSX element.
- Renders the UI based on props and state

**Syntax:**

```
import { Component } from "react";

class MyComponent extends Component {
  render() {
    return JSX;
  }
}
```

- using this.props in the render() body to access the props in a class components.

### 6. Can you explain why the component name should starts with uppercase?

- The component name should always start with a capital letter as react treats the components starting with lowercase letters as HTML elements.

### 7. Can you explain why we need ReactDOM?

- ReactDOM library is responsible for rendering React component in the browser's DOM(Document Object Model)
- When a React component is created, it is not immediately visible in the browser. Instead, it is a representation of a virtual DOM that is managed by React. In order to show the component in the browser, we need to use the ReactDOM library to render it.

Here's an example of how ReactDOM is used to render a React component:

```
import React from 'react';
import ReactDOM from 'react-dom';

function App() {
  return <h1>Hello, world!</h1>;
}

ReactDOM.render(<App />, document.getElementById('root'));
```

In this example, we create a functional component App that returns a h1 element with the text "Hello, world!". We then use ReactDOM.render() to render the App component into the HTML element with the id of "root".

### 8. Can you explain the difference between state and props?

**State:**

- The state is a JS object in which we store the component's data that changes over time.
- When the state object changes, the component re-renders.
  **Intialising State:**

```
class Counter extends Component {
  state = { count: 0 }
  render() {
    const { count } = this.state;
    return <p className="count">{count}</p>;
  }
}
```

**Props**

- React allows us to pass information to a component using props.
- We can pass props to any component as we declare attributes for any HTML element.

**Syntax**

`<Component propName1="propValue1" propName2="propValue2" />`

- The components accept props as parameters and can be accessed directly.

**Syntax**

```
const Component = (props) => {
  // We can access props here
};
```

### 9. Can you list all the difference between JSX and HTML?

**HTML**

- class
- for
- HTML tag names are case-insensitive.
- comments are written using the `<!-- -->` syntax.

**JSX**

- className
- htmlFor
- JSX tag names are always in camelCase
- comments are written using the JavaScript comment syntax `/* */ or //`

### 10. Can you explain why we use PropTypes?

- PropTypes is a way to validate the props passed to a component.
- It can catch catch errors early and provide better feedback to developers when incorrect props are passed. This can help prevent bugs and make code easier to maintain.

### 11 Can you explain this statement setState is asynchronous in nature with an example?

- We can update the state by using setState(). We can provide function/object as an argument to set the state.
- setState() is an asynchronous operation and does not update the state immediately after calling updates state in next render cycle.

**Syntax:**

`this.setState( prevState => ({... }) )`

**Example**

```
onIncrement = () => {
  this.setState((prevState) =>
    console.log(`previous state value ${prevState.count}`)
  )
}
```

Here the previous state is sent as a parameter to the callback function.

### 12. Can you explain why we should not update the state directly?

- In functional components, it's also important to never update the state directly by assigning a new value to the state variable. Instead, we should always use the useState() hook provided by React to update the state.

Here's an example of updating the state directly in a functional component:

```
import React, { useState } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  function handleClick() {
    count = count + 1; // updating state directly
    console.log(count);
  }

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={handleClick}>Increment</button>
    </div>
  );
}

```

updating the state directly like this can cause issues and bugs in your code

- the component may not re-render
- state updates may lost
  instead use setCount() function in useState() to update the state

### 13. Can you handle form validation in React?

- Yes,few ways to handle form validation in React, but one common approach is to use a combination of state and event handlers.

### 14.Can you explain what is Virtual DOM and how it works?

- The Virtual DOM is an important part of React's performance optimization, as it allows React to update the web page efficiently and with minimal disruption to the user experience.

**Example**

```
function MyList(props) {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item.id}>{item.name}</li>
      ))}
    </ul>
  );
}
```

- When the component first renders, React creates a Virtual DOM representation of the list and renders it to the actual DOM. If the user then adds a new item to the list, React updates the Virtual DOM with the new data and compares it to the previous version. It then determines that a new li element needs to be added to the actual DOM, and it does so without having to re-render the entire list.

### 15. Can you explain the difference between controlled and uncontrolled component?

**Controlled Components**

- Values are controlled by React

**Example**

```
import React, { useState } from "react";

function ControlledComponent() {
  const [value, setValue] = useState("");

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <input type="text" value={value} onChange={handleChange} />
  );
}
```

- In this example, the value of the input element is controlled by the value state variable, and the handleChange function updates the state and the value of the input whenever the user types in the field.

**UnControlled Components**

- Values are controlled by the DOM

**Example**

```
import React from "react";

function UncontrolledComponent() {
  return (
    <input type="text" defaultValue="Default value" />
  );
}
```

- In this example, the value of the input element is set using the defaultValue prop, and any changes made to the value of the input are updated directly in the DOM, rather than being stored in the component's state.

### 16. Can you explain why we need to pass key prop when displaying a list?

- Keys help React to identify which items have changed, added, or removed.
- They should be given to the elements inside the array for a stable identity.
- Without a key prop, React has to re-render the entire list every time there is a change, even if only a single item has been updated. This can lead to unnecessary re-renders and reduced performance.

**Example**

```
function ItemList(props) {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item.id}>{item.name}</li>
      ))}
    </ul>
  );
}
```

- In this example, the key prop is set to the id property of each item in the list, ensuring that each item has a unique identifier.
- Without a key prop, React has to re-render the entire list every time there is a change, even if only a single item has been updated. This can lead to unnecessary re-renders and reduced performance.

### 17. Can you explain different lifecycle methods and their use-cases?

Every React Component goes through three phases throughout its lifetime:

1. Mounting Phase

   - In this phase, the instance of a component is created and inserted into the DOM.
   - **constructor()** method is used to set up the initial state and class variables.
     **Syntax**

   ```
   constructor(props) {
   super(props)
   this.state = { date: props.date }
   }
   ```

   - We must call the super(props) method before any other statement. Calling super(props) makes sure that constructor() of the React.Component gets called and initializes the instance.
   - The **render()** method is used to return the JSX that is displayed in the UI.
   - The **componentDidMount()** method is used to run statements that require that the component is already placed in the DOM.

   **Example:** set timers, initiate API calls, etc.

2. Updating Phase

   - In this phase, the component is updated whenever there is a change in the component's state.
   - The **render()** method is called whenever there is a change in the component's state.

3. Unmounting phase

   - In this phase, the component instance is removed from the DOM.
   - The **componentWillUnmount()** method is used to cleanup activities performed.

   **Example:** clearing timers, cancelling API calls, etc.

### 18. Can you list the reasons to use context in React and how it works?

- Context is a mechanism that provides different Components and allows us to pass data without doing prop drilling.

**Syntax**

`React.createContext(INITIAL_VALUE)`

- It returns an object with different properties to update and access values from the context.
- The Context object provides two properties.
  - Consumer
  - Provider
- We can access the value in the Context using Consumer Component provided by the Context Object.

**Syntax**

```
<ContextObject.Consumer>
  {/*callback function*/}
</ContextObject.Consumer>
```

- We access the Consumer component using dot notation from the context object.
- We will give a callback function as children for Consumer Component.

### 19. Can you explain the use case of React.Fragment?

- The fragment is an alternate way to return a single JSX element. It groups a list of children without adding extra nodes to the DOM.

**Syntax**

```
const Welcome = () => (
   <>
     <h1>Hello, User</h1>
     <p>You are learning React</p>
   </>
)

export default Welcome
```

### 20. Can you explain if it is possible to loop inside the JSX give an example of how to do this?

-Yes, it is possible to loop inside JSX using JavaScript expressions. You can use any valid JavaScript expression inside curly braces {} in JSX, including loops.

Here is an example of how to loop inside JSX using a map function:

```
function ItemList(props) {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item.id}>{item.name}</li>
      ))}
    </ul>
  );
}
```

- In this example, we have an array of items that we want to render as list items. We use the map function to loop over the array and create a new list item for each item in the array. We set the key prop to the index of each item, which is a unique identifier for each item in the list.

### 21. Can you explain the advantages of using Hooks?

- React Hooks are special functions that are a new addition to React

They allow us to use state and other React features(lifecycle methods, context, etc) in the Function Components

- Easy to reuse logic between the components
- Components become simple and easy to understand
- Less lines of code
- No need of switching between Class & Function Components
- Developers and Companies are gradually adapting to React Hooks

### 22. Can you explain how to use useEffect?

- React provides a built-in hook called useEffect that allows executing logic after the component render

Using Effect Hook we can perform actions like,

- Making API Call
- Using Scheduler methods like setInterval( )
- DOM Manipulations, etc.

- useEffect hooks allows to perform side effects in functional components.

**Syntax**

`useEffect(setup, dependencies?)`

- setup function as an first argument which excetues after every render
- can pass array of dependencies as second argument to control when the effect is excuted

**Example**

```
import React, {useRef, useState, useEffect } from 'react';

function App() {
    const [time, setTime] = useState(0);
    const ref = useRef()
    useEffect(() => {
         ref.current = setInterval(() => {
            setTime(time => time + 1);
        }, 1000);

        return () => clearInterval(ref.current);
    }, []);
    return (
        <div>
            <p>{time} seconds</p>
            <button onClick={() => clearInterval(ref.current)}>Clear</button>
        </div>
    );
}
```

### 23. Can you list all the hooks we can use to manage state in React with example?

- useState- The useState hook allows you to add state to your functional components. It takes an initial value as its argument and returns an array with two elements: the current state value and a function to update the state.

- useEffect- The useEffect hook allows you to perform side effects in your components, such as fetching data or updating the DOM. It takes a function as its argument and runs it after every render.

- useContext- The useContext hook allows you to access context values in your components. It takes a context object as its argument and returns the current context value.

- useReducer- The useReducer hook allows you to manage complex state updates in your components. It takes a reducer function and an initial state value as its arguments, and returns an array with the current state value and a dispatch function to update the state.

### 24. Can you explain the difference between a DOM element and React element?

- A **DOM element** is a standard HTML element that you might see in the browser's HTML inspector, such as a div, p, or img. DOM elements are created and managed by the browser.
- A **React element** is a plain JavaScript object that represents a component or HTML element, and contains information about its type, props, and children.

When a React element is rendered, it is transformed into a tree of DOM elements that can be added to the document and it's what allows React to efficiently update the UI when state or props change.

One of the benefits of using React elements is that they can be manipulated in memory before being rendered to the DOM.

For example, you can create elements dynamically, conditionally render them based on state, or compose them together into more complex components.

### 25. Can you explain what happens when any runtime error happens in the JSX? How can we fix that issue?

- When a runtime error occurs in the JSX code, it will cause the component to break and render an error message in the browser console.
- This can happen if a variable is undefined, a function is called with incorrect arguments, or any other type of JavaScript error.
- To fix errors:
  - Check the browser console for the error message
  - console.log() values of variables and objects in the code
  - Use tools like React Developer Tools or Redux DevTools to inspect the component's state and props, and see how they are changing over time.

### 26.Can you explain the rules of hooks?

- Rules of Hooks

Rule 1: Hooks should be called only at the top level
Rule 2: Hooks should be called only inside React Function Components and Custom Hooks
Rule 3: Hooks should not be called conditionally

### 27. Can you explain why we can put hooks in a if statement?

- In React, Hooks should not be called conditionally. This is one of the rules of Hooks. However, there is a specific use case where Hooks can be used in an if statement. This is when you need to conditionally render a part of your component and you also need to use a Hook to manage the state of that part of the component.

### 28. Can you explain what is the advantage of using React.StrictMode?

- React.StrictMode is a development mode that can be enabled in a React application to help developers identify and fix potential problems in their code.
- When it's enabled, React will perform additional checks and warnings that are not performed in the normal mode.
- Improves debugging
- Improves performance

React.StrictMode is a good practice to adopt in any React application, as it can help improve code quality, performance, and future-proofing.

### 29. Can you create the production build of a React app?

- Yes, you can create a production build of a React app, which is a final version of your application that is optimized for performance and ready for deployment.
- To create a production build, you need to run a build command that will bundle your application's JavaScript, CSS, and HTML files into a single package that can be served by a web server.
- Once the build is complete, you can deploy the production build to a hosting provider or a server.
- The specific steps for creating a production build may vary depending on your project setup, but generally involve using a build tool like Webpack or Create React App to bundle your code and optimizing it for performance.

### 30. Can you explain the difference between Development and Production?

- Here are some key differences between a development environment and a production environment:

1. Purpose: The main purpose of a development environment is to allow developers to work on the code and test new features, while the purpose of a production environment is to run the application and make it available to users.

2. Configuration: A development environment is usually configured for ease of use and flexibility, with features like hot reloading and debugging tools enabled to make it easier for developers to work on the code. A production environment, on the other hand, is configured for performance and security, with features like caching, compression, and SSL enabled to optimize the application for end users.

3. Optimization: In a development environment, code may not be optimized for performance or size, since the focus is on development speed and flexibility. In a production environment, however, code is optimized to reduce load times and ensure efficient resource utilization.

4. Testing: In a development environment, testing is done by developers using tools like unit testing and integration testing. In a production environment, testing is done by users, and issues discovered in production can be more difficult to fix.

5. Access: A development environment is usually only accessible to developers, while a production environment is accessible to end users.

Overall, the key difference between development and production environments is that a development environment is designed for ease of use and flexibility, while a production environment is designed for performance, security, and reliability. It's important to ensure that the code and configuration used in the development environment is optimized and tested before deploying to production, to ensure a smooth and secure user experience.

