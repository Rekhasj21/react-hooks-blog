# React Hooks

- Hooks are a new feature in React which allow us to use React features without having to write a class.
- Allow us to reuse stateful logic.
- Call hooks at the top level of your component.
- There are lots of React Hooks some of the most commonly used hooks in React: useState, useEffect, useRef, useContext, useReducer, useCallback, useMemo and you can create your own custom Hooks.

## useState

useState is a React Hook that lets you add state to functional components.

**Syntax**

`const [state,setState] = useState(initialState)`

initialState - value that is given initially to state

**Returns**

useState returns an array with exactly two values:

1. The current state value
2. Function that lets you update the state.

**Example**

```
import { useState } from 'react';

function Counter() {
  const [count, setCount] = useState(0);

  function handleClick() {
    setCount(count + 1);
  }
  return (
    <button onClick={handleClick}>
          Count: {count}
    </button>
  );
}
```

Above example we use useState to create a counter. When the button is clicked the count gets updated each time.

## useEffect

- useEffect hooks allows to perform side effects in functional components.
- It includes fetching data, updating DOM.

**Syntax**

`useEffect(setup, dependencies?)`

- setup function as an first argument which excetues after every render
- can pass array of dependencies as second argument to control when the effect is excuted

**Returns**

- It returns undefined

**Example**

```
import React, {useRef, useState, useEffect } from 'react';

function App() {
    const [time, setTime] = useState(0);
    const ref = useRef()
    useEffect(() => {
         ref.current = setInterval(() => {
            setTime(time => time + 1);
        }, 1000);

        return () => clearInterval(ref.current);
    }, []);
    return (
        <div>
            <p>{time} seconds</p>
            <button onClick={() => clearInterval(ref.current)}>Clear</button>
        </div>
    );
}
```

Above we use useEffect to create timer when the component mounts by creating an interval that updates time every second.
Used return function to clear up the interval when component unmounts.

## useRef

useRef is a React Hook that lets you reference a value that's not needed for rendering

**Syntax**

`const ref = useRef(initialValue)`

initialValue: value that refers initially

**Returns**

- it returns an object with single property

**Example**

```
import React,{useRef} from 'react'

function FocusElement() {
    const inputRef = useRef(null)

    useEffect(() => {
        inputRef.current.focus()
    }, [])
    return (
        <div>
            <input ref={inputRef} type='text' />
        </div>
    )
}
```

- Above create a reference to input element and use the reference to focus the input when button clicked.
- Changing ref does not trigger a re-render.
- This can be useful for storing a reference to a DOM element or for storing any other mutable value.

## useContext

useContext is a React Hook that lets you read and subscribe to context from your component.
Context is a way to share data between components without having to pass props down through the component tree.

**Syntax**

`const value = useContext(SomeContext)`

- SomeContext: The context that you’ve previously created with createContext

**Returns**

- useContext returns the context value for the calling component

**Example**

```
import React, { useContext } from 'react';

const MyContext = React.createContext();

function Context() {
  const value = useContext(MyContext);

  return (
    <div>
      <p>{value}</p>
    </div>
  );
}

function App() {
  return (
    <MyContext.Provider value="Hello World!">
      <Context />
    </MyContext.Provider>
  );
}
```

- Above we create a object using React.createContext() and then use useContext to acces the value in our Context component.

## useReducer

- useReducer is a React Hook that lets you add a reducer to your component.
- It is similar to the useState hook but allows for more complex state changes using a reducer function.

**Syntax**

`const [state, dispatch] = useReducer(reducer, initialArg)`

- reducer function takes two arguments the current state and action object

**Return**

- It returns an updated state.

**Example**

```
import React, { useReducer } from 'react';

const reducer = (state, action) => {
    switch (action) {
        case 'Increment':
            return state + 1
        case 'Decrement':
            return state - 1
        case 'Reset':
            return initialState
        default:
            return state
    }
}
function Counter() {
    const [count, dispatch] = useReducer(reducer, 0)
    return (
        <>
            <div>Count: {count}</div>
            <div onClick={() => dispatch('Increment')}>Increment</div>
            <div onClick={() => dispatch('Decrement')}>Decrement</div>
            <div onClick={() => dispatch('Reset')}>Reset</div>
        </>
    )
}
```

- Above reducer function takes current state and action object and returns updated state based on action type.

## useCallback

- useCallback is a hook that will return a memoized version of the callback function that only changes if one of the dependencies has changed.
- Usefull when passing callbacks to optimized child components that rely on reference equality to prevent unnecessary renders.

**Syntax**

`const cachedFn = useCallback(fn, dependencies)`

fn - a function definition that you want to cache between re-renders.
dependencies -alist of dependencies including every value within your component that's used inside your function.

**Returns**

- Its returns the fn fucntion you have passed.

**Example**

```
import React, { useState, useCallback } from 'react';

function Form() {
  const [formData, setFormData] = useState({ name: '', email: '' });

  const handleSubmit = useCallback((event) => {
    event.preventDefault();
  }, []);

  const handleChange = useCallback((event) => {
    const { name, value } = event.target;
    setFormData((formData) => ({ ...formData, [name]: value }));
  }, []);

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" name="name" value={formData.name} onChange={handleChange} />
      </label>
      <label>
        Email:
        <input type="email" name="email" value={formData.email} onChange={handleChange} />
      </label>
      <button type="submit">Submit</button>
    </form>
  );
}
```

- Above we use useCallback to memoize the handleSubmit and handleChange functions and this ensures that they are not recreated on every re-render, which can improve performance

## useMemo

-useMemo is similary to useCallback hook but lets you cache the result of a invoked function between re-renders.

**Syntax**

`const cachedValue = useMemo(calculateValue,dependencies)`

- calculateValue takes no argument and returns what you wanted to calculate.
- dependencies includes every value within your component that’s used inside your calculation.

**Returns**

- It returns the result of calling calculateValue with no arguments.

**Example**

```
import React, { useState, useMemo } from 'react'

export default function App() {
    const [count1, setcount1] = useState(0)
    const [count2, setcount2] = useState(0)

    const handleAdd1 = () => {
        setcount1(count1 + 1)
    }

    const handleAdd2 = () => {
        setcount2(count2 + 1)
    }

    const isEven = useMemo(() => {
        let i = 0
        while (i < 200000000) i++
        return count1 % 2 === 0
    }, [count1])

    return (
        <>
            <div>
                <button onClick={handleAdd1}>Count one - {count1}</button>
                <span>{isEven ? 'Even' : 'Odd'}</span>
            </div>
            <button onClick={handleAdd2}>Count Two - {count2}</button>
        </>
    )
}
```
